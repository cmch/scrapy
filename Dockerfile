FROM debian:10-slim

ARG DEBIAN_FRONTEND=noninteractive

RUN apt update && apt install -y \
python3 \
python3-venv
